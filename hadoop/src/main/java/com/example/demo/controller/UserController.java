package com.example.demo.controller;
import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserMapper userMapper;

    @RequestMapping("/login")
    public AjaxResult login(User user, HttpSession httpSession){
        if(user == null || !StringUtils.hasLength(user.getUsername()) || !StringUtils.hasLength(user.getPassword())){
            return AjaxResult.fail(-1,"非法参数");//调用失败请求的统一格式返回
        }
        //查询数据库
        User targetUser = userMapper.login(user.getUsername());//根据用户名去查询数据库
        if(targetUser != null ){
            //根据用户名查询到用户 开始校验密码
            if(targetUser.getPassword().equals(user.getPassword())){
                //密码也是匹配的
                //存储一个会话
                httpSession.setAttribute(AppVariable.USER_SESSION_KEY,user);
                return AjaxResult.success(targetUser);
            }else{
                //密码没有匹配上
                return AjaxResult.fail(-1,"非法参数");
            }
        }
        return AjaxResult.fail(-1,"非法参数");
    }

    @RequestMapping("/reg")
    public AjaxResult reg(User user){
        if(user == null || !StringUtils.hasLength(user.getUsername()) || !StringUtils.hasLength(user.getPassword())){
            return AjaxResult.fail(-1,"非法参数");//调用失败请求的统一格式返回
        }
        int ret = userMapper.reg(user);//将用户插入到数据库
        return AjaxResult.success(ret);
    }

    @RequestMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        HttpSession httpSession = request.getSession(false);
        httpSession.removeAttribute(AppVariable.USER_SESSION_KEY);//移除当前会话
        return AjaxResult.success(null);
    }
}
