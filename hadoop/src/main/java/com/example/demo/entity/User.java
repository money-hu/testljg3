package com.example.demo.entity;

import lombok.Data;
// 定义一个User类，用于存储用户信息：id、用户名、密码
@Data
public class User {
    private Integer id;
    private String username;
    private String password;
}
